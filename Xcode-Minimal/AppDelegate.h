//
//  AppDelegate.h
//  Xcode-Minimal
//
//  Created by Moritz on 27.09.16.
//  Copyright © 2016 Moritz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

